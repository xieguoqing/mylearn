/*
 * @Author: your name
 * @Date: 2020-10-21 21:58:28
 * @LastEditTime: 2020-10-21 22:08:41
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \waijieProject\src\router\index.js
 */
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: ()=>import('@/views/grouping')
    }
  ]
})
